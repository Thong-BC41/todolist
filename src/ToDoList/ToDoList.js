import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../components/Container";
import { ToDoListDarkTheme } from "../themes/ToDoListDarkTheme";
import { ToDoListPrimaryTheme } from "../themes/ToDoListPrimaryTheme";
import { ToDoListLightTheme } from "../themes/ToDoListLightTheme";
import { Dropdown } from "../components/Dropdown";
import { arrTheme } from "../themes/dataTheme";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../components/Heading";
import { Label, Input, TextField } from "../components/TextField";
import { Button } from "../components/Button";
import { Table, Thead, Tr, Th, Td, Tbody } from "../components/Table";
import { connect } from "react-redux";
import { add_task, change_theme } from "../redux/types/ToDoListTypes";
import {
  addTaskAciton,
  backAction,
  changeThemeAciton,
  deleteAction,
  doneAction,
  editAction,
  uplateAction,
} from "../redux/actions/ToDoListActions";

class Todolist extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => task.done === false)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editAction(task));
                    }
                  );
                }}
                className="mx-2"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneAction(task.id));
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteAction(task.id));
                }}
                className="mx-2"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done === true)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(backAction(task.id));
                }}
                className="mx-2"
              >
                <i class="fa fa-backward"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteAction(task.id));
                }}
                className="mx-2"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  // handleChange = () => {
  //   let { name, value } = e.target.value;
  //   this.setState({
  //     [name]: value,
  //   });
  // };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  //phiên bản cũ từ react 16.0 trở xuống
  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              //dispatch value lên Reducer
              // this.props.dispatch({
              //   type: change_theme,
              //   themeId: value,
              // });
              this.props.dispatch(changeThemeAciton(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <TextField
            value={this.state.taskName}
            onChange={(e) => {
              this.setState({
                taskName: e.target.value,
              });
            }}
            name="taskName"
            label="Task name"
            className="w-50"
          />
          <Button
            onClick={() => {
              // lấy thông tin người dùng từ input
              let { taskName } = this.state;
              //tạo ra task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              //đưa task object lên redux thông qua phương thức dispatch
              this.props.dispatch(addTaskAciton(newTask));
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i> Add Task
          </Button>
          {this.state.disabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.dispatch(uplateAction(this.state.taskName));
              }}
              className="ml-2"
            >
              <i class="fa fa-upload"></i> Updata Task
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    disabled: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(uplateAction(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i class="fa fa-upload"></i> Updata Task
            </Button>
          )}

          <hr />
          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  //đây là lifecycle trả về props cũ va state cũ của componnent trước khi render(lifecycle chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id != this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(Todolist);
