import { arrTheme } from "../../themes/dataTheme";
import { ToDoListDarkTheme } from "../../themes/ToDoListDarkTheme";
import {
  add_task,
  back_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  uplate_task,
} from "../types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task 1", done: true },
    { id: 2, taskName: "task 2", done: false },
    { id: 3, taskName: "task 3", done: true },
    { id: 4, taskName: "task 4", done: false },
  ],
  taskEdit: { id: "", taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      //kiểm tra rỗng
      if (action.newTask.taskName.trim() === "") {
        alert("Nhập vào Task Name");
        return { ...state };
      }
      //kiểm tra đã tồn tại
      let taskListNew = [...state.taskList];
      let index = taskListNew.findIndex((task) => {
        return task.taskName == action.newTask.taskName;
      });
      if (index != -1) {
        alert(" task name đã tồn tại");
        return { ...state };
      }
      taskListNew.push(action.newTask);

      //xử xý xong gắn taskList mới vào taskList
      state.taskList = taskListNew;

      return { ...state };
    }
    case change_theme: {
      //tìm theme dựa vào themeid
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        //set lại theme cho state.themeToDoList
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      let newTaskList = [...state.taskList];
      let index = newTaskList.findIndex((task) => {
        return task.id == action.taskId;
      });
      if (index != -1) {
        newTaskList[index].done = true;
      }
      return { ...state, taskList: newTaskList };
    }
    case back_task: {
      let newtaskList = [...state.taskList];
      let index = newtaskList.findIndex((task) => {
        return task.id == action.taskId;
      });
      if (index != -1) {
        newtaskList[index].done = false;
      }
      return { ...state, taskList: newtaskList };
    }
    case delete_task: {
      let newtaskList = state.taskList.filter((task) => {
        return task.id != action.taskId;
      });
      return { ...state, taskList: newtaskList };
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    case uplate_task: {
      //cập nhật taskname mới vào taskname của taskedit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };
      let newtaskList = [...state.taskList];
      let index = newtaskList.findIndex((task) => {
        return task.id == state.taskEdit.id;
      });
      console.log(index);
      if (index !== -1) {
        newtaskList[index] = state.taskEdit;
      }
      state.taskList = newtaskList;
      state.taskEdit = { id: -1, taskName: "", done: false };

      return { ...state };
    }
    default:
      return { ...state };
  }
};
