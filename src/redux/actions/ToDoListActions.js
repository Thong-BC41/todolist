import { upload } from "@testing-library/user-event/dist/upload";
import {
  add_task,
  back_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  uplate_task,
} from "../types/ToDoListTypes";

export const addTaskAciton = (newTask) => {
  return {
    type: add_task,
    newTask,
  };
};
export const changeThemeAciton = (themeId) => {
  return {
    type: change_theme,
    themeId,
  };
};
export const doneAction = (taskId) => ({
  type: done_task,
  taskId,
});
export const backAction = (taskId) => ({
  type: back_task,
  taskId,
});
export const deleteAction = (taskId) => ({
  type: delete_task,
  taskId,
});
export const editAction = (task) => ({
  type: edit_task,
  task,
});
export const uplateAction = (taskName) => ({
  type: uplate_task,
  taskName,
});
